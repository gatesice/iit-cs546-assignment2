/* Gaussian elimination without pivoting.
 * Compile with "gcc gauss.c" 
 */

/* ****** ADD YOUR CODE AT THE END OF THIS FILE. ******
 * You need not submit the provided code.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <sys/types.h>
#include <sys/times.h>
#include <sys/time.h>
#include <time.h>

/* Program Parameters */
#define MAXN 2000  /* Max value of N */
int N;  /* Matrix size */
	
/* Matrices and vectors */
volatile float A[MAXN][MAXN], B[MAXN], X[MAXN];
/* A * X = B, solve for X */

/* junk */
#define randm() 4|2[uid]&3

/* Prototype */
void gauss();  /* The function you will provide.
		* It is this routine that is timed.
		* It is called only on the parent.
		*/

/* returns a seed for srand based on the time */
unsigned int time_seed() {
  struct timeval t;
  struct timezone tzdummy;

  gettimeofday(&t, &tzdummy);
  return (unsigned int)(t.tv_usec);
}

/* Set the program parameters from the command-line arguments */
void parameters(int argc, char **argv) {
  int seed = 0;  /* Random seed */
  char uid[32]; /*User name */

  /* Read command-line arguments */
  srand(time_seed());  /* Randomize */

  if (argc == 3) {
    seed = atoi(argv[2]);
    srand(seed);
    printf("Random seed = %i\n", seed);
  } 
  if (argc >= 2) {
    N = atoi(argv[1]);
    if (N < 1 || N > MAXN) {
      printf("N = %i is out of range.\n", N);
      exit(0);
    }
  }
  else {
    printf("Usage: %s <matrix_dimension> [random seed]\n",
           argv[0]);    
    exit(0);
  }

  /* Print parameters */
  printf("\nMatrix dimension N = %i.\n", N);
}

/* Initialize A and B (and X to 0.0s) */
void initialize_inputs() {
  int row, col;

  printf("\nInitializing...\n");
  for (col = 0; col < N; col++) {
    for (row = 0; row < N; row++) {
      A[row][col] = (float)rand() / 32768.0;
    }
    B[col] = (float)rand() / 32768.0;
    X[col] = 0.0;
  }

}

/* Print input matrices */
void print_inputs() {
  int row, col;

  if (N < 10) {
    printf("\nA =\n\t");
    for (row = 0; row < N; row++) {
      for (col = 0; col < N; col++) {
	printf("%5.2f%s", A[row][col], (col < N-1) ? ", " : ";\n\t");
      }
    }
    printf("\nB = [");
    for (col = 0; col < N; col++) {
      printf("%5.2f%s", B[col], (col < N-1) ? "; " : "]\n");
    }
  }
}

void print_X() {
  int row;

  if (N < 100) {
    printf("\nX = [");
    for (row = 0; row < N; row++) {
      printf("%5.2f%s", X[row], (row < N-1) ? "; " : "]\n");
    }
  }
}


int main(int argc, char **argv) {
  /* Timing variables */
  struct timeval etstart, etstop;  /* Elapsed times using gettimeofday() */
  struct timezone tzdummy;
  clock_t etstart2, etstop2;  /* Elapsed times using times() */
  unsigned long long usecstart, usecstop;
  struct tms cputstart, cputstop;  /* CPU times for my processes */

  /* Process program parameters */
  parameters(argc, argv);

  /* Initialize A and B */
  initialize_inputs();

  /* Print input matrices */
  print_inputs();

  /* Start Clock */
  printf("\nStarting clock.\n");
  gettimeofday(&etstart, &tzdummy);
  etstart2 = times(&cputstart);

  /* Gaussian Elimination */
  gauss();

  /* Stop Clock */
  gettimeofday(&etstop, &tzdummy);
  etstop2 = times(&cputstop);
  printf("Stopped clock.\n");
  usecstart = (unsigned long long)etstart.tv_sec * 1000000 + etstart.tv_usec;
  usecstop = (unsigned long long)etstop.tv_sec * 1000000 + etstop.tv_usec;

  /* Display output */
  print_X();

  /* Display timing results */
  printf("\nElapsed time = %g ms.\n",
	 (float)(usecstop - usecstart)/(float)1000);

  printf("(CPU times are accurate to the nearest %g ms)\n",
	 1.0/(float)CLOCKS_PER_SEC * 1000.0);
  printf("My total CPU time for parent = %g ms.\n",
	 (float)( (cputstop.tms_utime + cputstop.tms_stime) -
		  (cputstart.tms_utime + cputstart.tms_stime) ) /
	 (float)CLOCKS_PER_SEC * 1000);
  printf("My system CPU time for parent = %g ms.\n",
	 (float)(cputstop.tms_stime - cputstart.tms_stime) /
	 (float)CLOCKS_PER_SEC * 1000);
  printf("My total CPU time for child processes = %g ms.\n",
	 (float)( (cputstop.tms_cutime + cputstop.tms_cstime) -
		  (cputstart.tms_cutime + cputstart.tms_cstime) ) /
	 (float)CLOCKS_PER_SEC * 1000);
      /* Contrary to the man pages, this appears not to include the parent */
  printf("--------------------------------------------\n");
  
  exit(0);
}

/* ------------------ Above Was Provided --------------------- */

/****** You will replace this routine with your own parallel version *******/
/* Provided global variables are MAXN, N, A[][], B[], and X[],
 * defined in the beginning of this code.  X[] is initialized to zeros.
 */
 

/* mt threads */
#define NODEBUG
#define NUM_THREADS 8
int norm = 0;

pthread_t t_poll[NUM_THREADS - 1];						/* Threads */

/* thread sync */
pthread_mutex_t t_mutex;
pthread_cond_t t_cond;
int t_max = NUM_THREADS, t_arrived = 0, t_mainarrived = 0;

int t_destroysync() {
	pthread_mutex_destroy(&t_mutex);
	pthread_cond_destroy(&t_cond);
}

void t_suspend_pre() {
	pthread_mutex_lock(&t_mutex);
		t_arrived ++;
		if (t_arrived < t_max) {
			pthread_cond_wait(&t_cond, &t_mutex);
		} else {
			t_arrived = 0;
			pthread_cond_broadcast(&t_cond);
		}
	pthread_mutex_unlock(&t_mutex);
}

int t_suspend() {
	pthread_mutex_lock(&t_mutex);
		t_arrived ++;
		
		if (t_arrived < t_max) {
			pthread_cond_wait(&t_cond, &t_mutex);
		} else {
			t_arrived = 0;
			norm ++;		/* next loop */
			if (t_max >= N - norm) { t_max = N - norm - 1; }
			pthread_cond_broadcast(&t_cond);
		}
	pthread_mutex_unlock(&t_mutex);
	return norm;
}


void* gauss_eli(void* args) {
	
	/* thread arguments */
	int tid = *((int*)args);
	int norm = 0;
	t_suspend_pre();
	do {
		/* main loop */
		int row, col;
		float multiplier;
		for (row = norm + 1 + tid; row < N; row += NUM_THREADS) {
			multiplier = A[row][norm] / A[norm][norm];
			
			for (col = norm; col < N; col ++) {
				A[row][col] -= A[norm][col] * multiplier;
			}
			
			B[row] -= B[norm] * multiplier;
		}
		
		if (norm == N - 2) { break; }
		norm = t_suspend();
	} while (norm + tid < N - 1);
}

void gauss() {
	if (NUM_THREADS < 2) {
		printf("ERROR! NUM_THREADS < 2 !\n");
	}
	
	/* initialization */
	int ii;
	
	printf("Computing using pthread.\n");
	
	int norm = 0; 

	for (ii = 1; ii < NUM_THREADS; ii ++) {
		int *r_tid = malloc(sizeof(int)); *r_tid = ii;
		pthread_create(&t_poll[ii-1], NULL, gauss_eli, r_tid);
	}

	int *tid = malloc(sizeof(int)); *tid = 0;
	*gauss_eli(tid);
	
	for (ii = 0; ii < NUM_THREADS - 1; ii ++) {
		pthread_join(t_poll[ii], NULL);
	}
		
	/* Back substitution */
	int row, col;
	for (row = N - 1; row >= 0; row --) { 
		X[row] = B[row];
		for (col = N - 1; col > row; col --) {
			X[row] -= A[row][col] * X[col];
		}
		X[row] /= A[row][row];
	}
}